import { Article } from '../pages';

const ArticleCard: React.FunctionComponent<Article> = ({ id, title, body }) => {
	return (
		<div className="border border-slate-300 p-3 rounded-md hover:scale-110 transition duration-300 bg-white cursor-pointer" key={id}>
			<h5 className="text-sky-600 font-semibold text-lg">{title}</h5>
			<p className="mt-2">{body}</p>
		</div>
	);
};

export default ArticleCard;
