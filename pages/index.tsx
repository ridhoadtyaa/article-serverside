import type { NextPage } from 'next';
import ArticleCard from '../components/ArticleCard';

export interface Article {
	userId: number;
	id: number;
	title: string;
	body: string;
}

interface HomeProps {
	articles: Article[];
}

const Home: NextPage<HomeProps> = ({ articles }) => {
	return (
		<>
			<header className="layout py-4">
				<h1 className="text-center text-2xl font-semibold text-sky-500">Articles</h1>
			</header>
			<main className="layout my-8">
				<div className="grid grid-cols-1 md:grid-cols-2 gap-4">
					{articles.map(article => (
						<ArticleCard {...article} key={article.id} />
					))}
				</div>
			</main>
		</>
	);
};

export const getServerSideProps = async () => {
	const res = await fetch('https://jsonplaceholder.typicode.com/posts');
	const articles = await res.json();

	return { props: { articles } };
};

export default Home;
